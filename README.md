# Zalo Zns

## Contents of this file

- Introduction
- Requirements
- Configuration
- Maintainers

## Introduction

Integrate [Zalo Zns](https://zalo.cloud/zns) service to send Zns notification.

## Requirements

This module requires Drupal core >= 10.0.

"zaloplatform/zalo-php-sdk": "^4.0"
(have to apply patch fix_deprecated_in_zalo_sdk.patch)

Optional modules: drupal/restui, drupal/message_notify

## Configuration

### Set up Zalo ZNS

- https://zalo.cloud/zns/guidelines/set-up

### Set up on Drupal

- Config App ID, App secret, OA secret key (Optional, for webhook),
  Development mode at /admin/config/system/zalo-zns-settings
- or set app_id, app_secret, oa_secret_key and development_mode
  configuration in settings.php
  ```php
    $config['zalo_zns.settings']['app_id'] = '21365**********387';
    $config['zalo_zns.settings']['app_secret'] = '457k*********vnkh';
    $config['zalo_zns.settings']['development_mode'] = TRUE;

    $config['zalo_zns.settings']['oa_secret_key'] = 'AMX*******4fHgZ';
    // Optional, for webhook
  ```
- Enable rest resource `Zalo OA Callback`, allow anonymous access.
- Go to /zalo-zns/zalo-access-token-request to get link to request access token.
- Click on the generated link, it will redirect to Zalo page to login and
  grant permission.
- After that, Zalo will send request to webhook `Zalo OA Callback` and
  access token will be stored in system.
- Use service `highlands_coffee_egift.helper` call func `sendZnsNotification`
  to send ZNS notification.

### Use Module Message and Message Notify to Send and Manage ZNS notifications.

- Create a message template.
- Add Text(plain) field,
  machine name `field_send_to` to store the notification destination.
- Add List(text) field, machine name `field_status` with allowed values
  as below to store the status of notification.
  ```php
    draft|Draft
    sent|Sent
    success|Success
    failed|Failed
  ```
- Add parameter fields, Text(plain) fields, Ex. `field_name`, `field_code`, ....
- In `Message Text`, set the template as below:
  ```php
    template_id|293305
    template_data.name|[message:field_name:value]
    template_data.code|[message:field_code:value]
  ```
- `name` and `code` in example above are mapping with template parameters in
  registered ZNS template.
- Example Code to notify Zns:
  ```php
    /** @var \Drupal\message_notify\MessageNotifier $notifier */
    $notifier = \Drupal::service('message_notify.sender');
    $message = Message::create([
      'template' => <template_id>,
      'field_send_to' => 84937662221,
      // Must be a Vietnamese phone number in format
      'field_name' => 'Du Giang',
      'field_code' => 'abcd1234',
    ]);
    $message->save();
    $notifier->send($message, [], 'zalo_zns');
  ```
### Webhook update status after user receive ZNS message notification
- Enable rest resource `Zalo Notification Webhook`, allow anonymous access.
- Config `OA secret key` at /admin/config/system/zalo-zns-settings
- or set app_id, app_secret, oa_secret_key & development_mode in settings.php
  ```php
    $config['zalo_zns.settings']['oa_secret_key'] = 'AMX*******4fHgZ';
  ```

### Cron job
- Enable Default cron Handler of Zalo ZNS to renew refresh token weekly.

## Maintainers

Current maintainers:

- Du Giang ([@sangdu2326](https://www.drupal.org/u/sangdu2326))
