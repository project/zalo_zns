<?php

namespace Drupal\zalo_zns;

/**
 * PKCE Util funtions.
 *
 * @package Zalo
 */
class PKCEUtil {

  /**
   * Generates code verifier.
   *
   * @return string
   *   Code verifier.
   */
  public static function genCodeVerifier() {
    $random = bin2hex(openssl_random_pseudo_bytes(32));
    return self::base64urlEncode(pack('H*', $random));
  }

  /**
   * Generates code challenge.
   *
   * @param string $codeVerifier
   *   Code verifier.
   *
   * @return string
   *   Code challenge.
   */
  public static function genCodeChallenge(string $codeVerifier) {
    return self::base64urlEncode(pack('H*', hash('sha256', $codeVerifier)));
  }

  /**
   * Base64 url encode.
   */
  private static function base64urlEncode($plainText) {
    $base64 = base64_encode($plainText);
    $base64 = trim($base64, "=");
    return strtr($base64, '+/', '-_');
  }

}
