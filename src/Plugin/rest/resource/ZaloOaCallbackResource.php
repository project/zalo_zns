<?php

declare(strict_types = 1);

namespace Drupal\zalo_zns\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\zalo_zns\Helper;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zalo\Zalo;

/**
 * Represents Zalo OA Callback records as resources.
 *
 * @RestResource (
 *   id = "zalo_zns_zalo_oa_callback",
 *   label = @Translation("Zalo OA Callback"),
 *   uri_paths = {
 *     "canonical" = "/api/zalo-zns-zalo-oa-callback",
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively, you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
final class ZaloOaCallbackResource extends ResourceBase {

  /**
   * The key-value storage.
   */
  private readonly KeyValueStoreInterface $storage;

  /**
   * The expirable key-value storage.
   */
  private readonly KeyValueStoreExpirableInterface $expirableStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    KeyValueFactoryInterface $keyValueFactory,
    KeyValueExpirableFactoryInterface $keyValueExpirableFactory,
    protected Helper $zaloZnsHelper,
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelFactory $loggerFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->storage = $keyValueFactory->get(Helper::ZALO_ACCESS_TOKEN_STORAGE);
    $this->expirableStorage = $keyValueExpirableFactory->get(Helper::ZALO_ACCESS_TOKEN_STORAGE);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue'),
      $container->get('keyvalue.expirable'),
      $container->get('zalo_zns.helper'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
    );
  }

  /**
   * Responds to GET requests.
   */
  public function get(): ResourceResponse {
    try {
      $zaloSettings = $this->configFactory->get('zalo_zns.settings');
      $zalo = new Zalo([
        'app_id' => (string) $zaloSettings->get('app_id'),
        'app_secret' => $zaloSettings->get('app_secret'),
      ]);
      $helper = $zalo->getRedirectLoginHelper();

      $codeVerifier = $this->storage->get('code_verifier');
      $zaloToken = $helper->getZaloTokenByOA($codeVerifier);
      $this->zaloZnsHelper->storeZaloToken($zaloToken);
      $this->loggerFactory->get('zalo_zns')->info('Zalo OA Callback: Access Token is Updated');
    }
    catch (\Exception $exception) {
      $this->loggerFactory->get('zalo_zns')->error('Zalo OA Callback: ' . $exception->getMessage());
      return new ResourceResponse('Failed');
    }
    return new ResourceResponse('Ok');
  }

}
