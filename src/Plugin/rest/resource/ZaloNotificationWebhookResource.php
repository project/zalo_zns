<?php

namespace Drupal\zalo_zns\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\message\Entity\Message;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Represents Zalo Notification Webhook records as resources.
 *
 * @RestResource (
 *   id = "zalo_zns_zalo_notification_webhook",
 *   label = @Translation("Zalo Notification Webhook"),
 *   uri_paths = {
 *     "create" = "/api/zalo-zns-zalo-notification-webhook"
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively, you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
class ZaloNotificationWebhookResource extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelFactory $loggerFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   */
  public function post(Request $request, array $data): ModifiedResourceResponse {
    $server = $request->headers->get('X-Zevent-Server');
    $signature = $request->headers->get('X-Zevent-Signature');

    if ($server == 'ZNS') {
      $config = $this->configFactory->get('zalo_zns.settings');
      $logger = $this->loggerFactory->get('zalo_zns');
      $appId = (string) $config->get('app_id');
      $oaSecretKey = $config->get('oa_secret_key');
      $jsonEncode = json_encode($data);
      $verifySignature = 'mac=' . hash('sha256', $appId . $jsonEncode . $data['timestamp'] . $oaSecretKey);
      if ($verifySignature == $signature) {
        if ($data['event_name'] == 'user_received_message') {
          $message = $this->entityTypeManager->getStorage('message')->load($data['message']['tracking_id']);
          if ($message) {
            $message->set('field_status', 'success')->save();
            $logger->info("User received zns {$message->id()} successfully");
          }
        }
      }
      else {
        $logger->error("X-Zevent-Signature is not match");
      }
    }

    return new ModifiedResourceResponse('OK', 200);
  }

}
