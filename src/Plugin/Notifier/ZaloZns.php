<?php

namespace Drupal\zalo_zns\Plugin\Notifier;

use Drupal\Core\Render\Markup;
use Drupal\message_notify\Plugin\Notifier\MessageNotifierBase;

/**
 * Zalo Zns notifier.
 *
 * @Notifier(
 *   id = "zalo_zns",
 *   title = @Translation("Zalo Zns"),
 *   description = @Translation("Send messages via Zalo Zns"),
 *   viewModes = {
 *     "mail_body"
 *   }
 * )
 */
class ZaloZns extends MessageNotifierBase {

  const NOTIFIER_NAME = 'zalo_zns';

  /**
   * {@inheritdoc}
   */
  public function deliver(array $output = []) {
    $developmentMode = \Drupal::config('zalo_zns.settings')->get('development_mode');
    if (isset($output['mail_body']) && $this->message?->field_send_to?->value) {
      $body = $output['mail_body'];
      if ($body instanceof Markup) {
        $body = $body->__toString();
      }
      $phoneNo = $this->message?->field_send_to?->value;
      if (str_starts_with($phoneNo, '0')) {
        $phoneNo = '84' . substr($phoneNo, 1);
      }
      $body = trim(strip_tags($body));
      $bodyItems = explode(PHP_EOL, $body);
      $templateId = NULL;
      $templateData = [];
      foreach ($bodyItems as $bodyItem) {
        $bodyItem = trim($bodyItem);
        $explodes = explode('|', $bodyItem);
        $key = reset($explodes);
        $value = end($explodes);
        if ($key == 'template_id') {
          $templateId = $value;
        }
        elseif (str_starts_with($key, 'template_data.')) {
          $key = substr($key, 14);
          $templateData[$key] = $value;
        }
      }

      /** @var \Drupal\zalo_zns\Helper $zaloZnsHelper */
      $zaloZnsHelper = \Drupal::service('zalo_zns.helper');

      // Send request.
      try {
        $response = $zaloZnsHelper->sendZnsNotification($developmentMode, $phoneNo, $templateId, $templateData, $this->message->id());
        $this->message->set('field_status', 'sent')->save();
        $body = json_decode($response->getBody()->getContents(), TRUE);
        if (empty($body['error'])) {
          \Drupal::logger('zalo_zns')->info("Sent zns {$this->message->id()} successfully");
          return TRUE;
        }
        else {
          throw new \Exception($body['message'] ?? '');
        }
      }
      catch (\Exception $exception) {
        $this->message->set('field_status', 'failed')->save();
        \Drupal::logger('zalo_zns')->error("Sent zns {$this->message->id()} failed: " . $exception->getMessage());
        return FALSE;
      }
    }
    $this->message->set('field_status', 'failed')->save();
    \Drupal::logger('zalo_zns')->error("Sent zns {$this->message->id()} failed");
    return FALSE;
  }

}
