<?php

namespace Drupal\zalo_zns\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Zalo Zns settings for this site.
 */
final class ZaloZnsSettingsForm extends ConfigFormBase {

  /**
   * Zalo ZNS config with override.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $zaloConfigWithOverride;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->zaloConfigWithOverride = $this->configFactory->get('zalo_zns.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'zalo_zns_zalo_zns_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['zalo_zns.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['development_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Development Mode'),
      '#default_value' => $this->config('zalo_zns.settings')->get('development_mode'),
    ];
    if ($this->zaloConfigWithOverride->hasOverrides('development_mode')) {
      $form['development_mode']['#default_value'] = FALSE;
      $form['development_mode']['#description'] = $this->t('Overridden by settings.php or module');
      $form['development_mode']['#attributes'] = [
        'disabled' => TRUE,
      ];
    }

    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#default_value' => $this->config('zalo_zns.settings')->get('app_id'),
      '#required' => TRUE,
    ];
    if ($this->zaloConfigWithOverride->hasOverrides('app_id')) {
      $form['app_id']['#default_value'] = '';
      $form['app_id']['#description'] = $this->t('Overridden by settings.php or module');
      $form['app_id']['#required'] = FALSE;
      $form['app_id']['#attributes'] = [
        'disabled' => TRUE,
      ];
    }

    $form['app_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Secret'),
      '#default_value' => $this->config('zalo_zns.settings')->get('app_secret'),
      '#required' => TRUE,
    ];
    if ($this->zaloConfigWithOverride->hasOverrides('app_secret')) {
      $form['app_secret']['#default_value'] = '';
      $form['app_secret']['#description'] = $this->t('Overridden by settings.php or module');
      $form['app_secret']['#required'] = FALSE;
      $form['app_secret']['#attributes'] = [
        'disabled' => TRUE,
      ];
    }

    $form['oa_secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OA Secret Key'),
      '#default_value' => $this->config('zalo_zns.settings')->get('oa_secret_key'),
      '#description' => $this->t('Use for OA Notification Service Webhook'),
      '#required' => TRUE,
    ];
    if ($this->zaloConfigWithOverride->hasOverrides('oa_secret_key')) {
      $form['oa_secret_key']['#default_value'] = '';
      $form['oa_secret_key']['#description'] = $this->t('Overridden by settings.php or module');
      $form['oa_secret_key']['#required'] = FALSE;
      $form['oa_secret_key']['#attributes'] = [
        'disabled' => TRUE,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('zalo_zns.settings')
      ->set('development_mode', $form_state->getValue('development_mode') ?? $this->config('zalo_zns.settings')->get('development_mode'))
      ->set('app_id', $form_state->getValue('app_id') ?? $this->config('zalo_zns.settings')->get('app_id'))
      ->set('app_secret', $form_state->getValue('app_secret') ?? $this->config('zalo_zns.settings')->get('app_secret'))
      ->set('oa_secret_key', $form_state->getValue('oa_secret_key') ?? $this->config('zalo_zns.settings')->get('oa_secret_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
