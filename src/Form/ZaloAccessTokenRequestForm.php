<?php

namespace Drupal\zalo_zns\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\zalo_zns\Helper;
use Drupal\zalo_zns\PKCEUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zalo\Zalo;

/**
 * Form to request zalo access token.
 */
class ZaloAccessTokenRequestForm extends ConfirmFormBase {

  /**
   * The key-value storage.
   */
  private readonly KeyValueStoreInterface $storage;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(KeyValueFactoryInterface $keyValueFactory) {
    $this->storage = $keyValueFactory->get(Helper::ZALO_ACCESS_TOKEN_STORAGE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'zalo_zns_zalo_access_token_request';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Information to request new Zalo access token');
  }

  /**
   * {@inheritDoc}
   */
  public function getConfirmText() {
    return $this->t('Get Link To Renew Access Token');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('system.admin_config');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $routeZaloCallback = 'rest.zalo_zns_zalo_oa_callback.GET';
    $callbackUrl = Url::fromRoute($routeZaloCallback)->setAbsolute()->toString();
    $state = 'zalo_code_challenge';

    $codeVerifier = PKCEUtil::genCodeVerifier();
    $codeChallenge = PKCEUtil::genCodeChallenge($codeVerifier);

    $this->storage->set('code_verifier', $codeVerifier);

    $zaloSettings = \Drupal::config('zalo_zns.settings');
    $zalo = new Zalo([
      'app_id' => (string) $zaloSettings->get('app_id'),
      'app_secret' => $zaloSettings->get('app_secret'),
    ]);
    $helper = $zalo->getRedirectLoginHelper();
    $loginUrl = $helper->getLoginUrlByOA($callbackUrl, $codeChallenge, $state);

    $this->messenger()->addStatus($this->t('<a target="_blank" href="@link">@link</a>', [
      '@link' => $loginUrl,
    ]));
  }

}
