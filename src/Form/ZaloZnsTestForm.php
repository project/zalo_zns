<?php

namespace Drupal\zalo_zns\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\message\Entity\Message;
use Drupal\zalo_zns\Helper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Zalo Zns form.
 */
final class ZaloZnsTestForm extends FormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Zalo ZNS helper.
   *
   * @var \Drupal\zalo_zns\Helper
   */
  protected Helper $zaloZnsHelper;

  /**
   * {@inheritDoc}
   */
  public function __construct(ModuleHandlerInterface $module_handler, Helper $zaloZnsHelper) {
    $this->moduleHandler = $module_handler;
    $this->zaloZnsHelper = $zaloZnsHelper;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('zalo_zns.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'zalo_zns_zalo_zns_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $option = ['send_directly' => $this->t('Send directly')];
    if ($this->moduleHandler->moduleExists('message_notify')) {
      $option['message_notify'] = $this->t('Message notify');
    }
    $form['method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Method'),
      '#description' => $this->t('When select "Message notify",<br>development mode base on config in settings.php and can not override access token.'),
      '#options' => $option,
      '#default_value' => 'send_directly',
    ];
    $form['dev_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Development mode'),
      '#default_value' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="method"]' => ['value' => 'send_directly'],
        ],
      ],
    ];
    $form['phone_no'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Phone number'),
      '#required' => TRUE,
      '#rows' => 2,
      '#default_value' => $form_state->getValue('phone_no') ?? '',
      '#description' => $this->t("Enter phone number separated by comma (,)"),
    ];
    $form['template_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Template ID'),
      '#default_value' => $form_state->getValue('template_id') ?? '',
      '#description' => $this->t('If Method is "Send directly", it should be ZNS template id from Zalo ZNS.<br>If Method is "Message notify", it should be message template id from Drupal system.'),
      '#required' => TRUE,
    ];
    $form['template_data'] = [
      '#type' => 'textarea',
      '#default_value' => $form_state->getValue('template_data') ?? '',
      '#title' => $this->t('Template data'),
      '#required' => TRUE,
      '#description' => $this->t('One parameter per row, format: key|value<br>If Method is "Send directly", key should be ZNS template parameter from Zalo ZNS.<br>If Method is "Message notify", it should be machine name of message template field from Drupal system.'),
    ];
    $form['override_access_token'] = [
      '#type' => 'textarea',
      '#rows' => 2,
      '#title' => $this->t('Override access token'),
      '#default_value' => $form_state->getValue('override_access_token') ?? '',
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="method"]' => ['value' => 'send_directly'],
        ],
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->setRebuild(TRUE);

    $method = $form_state->getValue('method');
    $rawTemplateData = $form_state->getValue('template_data');
    $templateData = [];
    foreach (explode(PHP_EOL, $rawTemplateData) as $datum) {
      [$key, $value] = explode('|', trim($datum));
      $templateData[$key] = $value;
    }
    $phoneNumbers = array_map('trim', str_replace(['.', '-', ' '], '', explode(',', $form_state->getValue('phone_no'))));
    $templateId = $form_state->getValue('template_id');

    $messageNotifyExists = $this->moduleHandler->moduleExists('message_notify');
    // @phpstan-ignore-next-line
    $notifierService = $messageNotifyExists ? \Drupal::service('message_notify.sender') : NULL;

    foreach ($phoneNumbers as $phoneNo) {
      if ($method == 'send_directly') {
        try {
          $response = $this->zaloZnsHelper->sendZnsNotification($form_state->getValue('dev_mode'), $phoneNo, $templateId, $templateData, NULL, $form_state->getValue('override_access_token'));
          $result = $response->getBody()->getContents();
          $this->messenger()->addStatus($this->t('The message has been sent. Data: <br><em>@json</em>', ['@json' => $result]));
        }
        catch (\Exception $exception) {
          $this->messenger()->addError($this->t('The message has not been sent. Message: @message', ['@message' => $exception->getMessage()]));
          $this->messenger()->addError($exception->getMessage());
        }
      }
      elseif ($method == 'message_notify' && $messageNotifyExists && $notifierService) {
        $message = Message::create([
          'template' => $templateId,
          'field_send_to' => $phoneNo,
        ] + $templateData);
        $message->save();
        $result = $notifierService->send($message, [], 'zalo_zns');
        $this->messenger()->addStatus($this->t('The message has been sent. Result: @result', ['@result' => $result ? 'Success' : 'Failed']));
      }
    }
  }

}
