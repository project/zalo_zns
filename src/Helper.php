<?php

namespace Drupal\zalo_zns;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Zalo\Authentication\ZaloToken;
use Zalo\Zalo;

/**
 * Zalo ZNS functions.
 */
class Helper {

  const ZALO_ACCESS_TOKEN_STORAGE = 'zalo_access_token_storage';
  const ZALO_SEND_ZNS_API = 'https://business.openapi.zalo.me/message/template';

  /**
   * The expirable key-value storage.
   */
  private readonly KeyValueStoreExpirableInterface $expirableStorage;

  /**
   * Logger.
   */
  private LoggerInterface $logger;

  /**
   * Zalo ZNS config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $zaloZnsConfig;

  /**
   * Constructs a Helper object.
   */
  public function __construct(
    KeyValueExpirableFactoryInterface $keyValueExpirableFactory,
    protected Client $client,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    ConfigFactory $configFactory,
  ) {
    $this->expirableStorage = $keyValueExpirableFactory->get(self::ZALO_ACCESS_TOKEN_STORAGE);
    $this->logger = $loggerChannelFactory->get('zalo_zns');
    $this->zaloZnsConfig = $configFactory->get('zalo_zns.settings');
  }

  /**
   * Get Zalo access token.
   */
  public function getAccessToken() {
    $accessToken = $this->expirableStorage->get('access_token');
    if ($accessToken) {
      return $accessToken;
    }
    else {
      $zalo = new Zalo([
        'app_id' => (string) $this->zaloZnsConfig->get('app_id'),
        'app_secret' => $this->zaloZnsConfig->get('app_secret'),
      ]);
      try {
        $zaloToken = $zalo->getOAuth2Client()
          ->getZaloTokenFromRefreshTokenByOA($this->expirableStorage->get('refresh_token'));
        $this->storeZaloToken($zaloToken);
        $this->logger->info('getZaloTokenFromRefreshTokenByOA: Access Token is Updated');
        return $zaloToken->getAccessToken();
      }
      catch (\Exception $exception) {
        $this->logger->error('getZaloTokenFromRefreshTokenByOA: ' . $exception->getMessage());
      }
    }
    return NULL;
  }

  /**
   * Store zalo token into expirable storage.
   *
   * @param \Zalo\Authentication\ZaloToken $zaloToken
   *   Zalo token object.
   *
   * @return void
   *   Void.
   */
  public function storeZaloToken(ZaloToken $zaloToken) {
    $this->expirableStorage->setWithExpire('access_token', $zaloToken->getAccessToken(), $zaloToken->getAccessTokenExpiresAt()->getTimestamp() - time() - 3600);
    $this->expirableStorage->setWithExpire('refresh_token', $zaloToken->getRefreshToken(), 7776000);
  }

  /**
   * Send ZNS notification API call.
   *
   * @param bool|int $developmentMode
   *   Development mode flag.
   * @param string $phoneNo
   *   Phone number.
   * @param string $templateId
   *   Zns template ID.
   * @param array $templateData
   *   Zns template data.
   * @param string|null $trackingId
   *   System tracking ID.
   * @param string|null $overrideAccessToken
   *   Override access token for testing.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Zns response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendZnsNotification(bool|int $developmentMode, string $phoneNo, string $templateId, array $templateData, string|null $trackingId = NULL, string|null $overrideAccessToken = NULL): ResponseInterface {
    $payload = [
      'phone' => $phoneNo,
      'template_id' => $templateId,
      'template_data' => $templateData,
    ];
    if ($developmentMode) {
      $payload['mode'] = 'development';
    }
    if ($trackingId) {
      $payload['tracking_id'] = $trackingId;
    }
    return $this->client->post(self::ZALO_SEND_ZNS_API, [
      RequestOptions::BODY => json_encode($payload),
      RequestOptions::HEADERS => [
        'access_token' => $overrideAccessToken ?? $this->getAccessToken(),
      ],
    ]);
  }

}
